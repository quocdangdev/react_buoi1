import React, { Component } from 'react'
import CartItem from './CartItem'

export default class CartList extends Component {
    renderTbody = (handelChangeCart) => this.props.cart.map((item) => (<CartItem key={item.id} item={item} handelChangeCart={handelChangeCart} />))
    render() {
        const { handelChangeCart } = this.props
        return (
            <table className='table table-sm table-dark' >
                <thead>
                    <tr>
                        <td>id</td>
                        <td>name</td>
                        <td>img</td>
                        <td>price</td>
                        <td>quantity</td>
                    </tr>
                </thead>
                <tbody>
                    {this.renderTbody(handelChangeCart)}
                </tbody>
            </table>
        )
    }
}
