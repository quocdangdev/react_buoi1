import React, { Component } from 'react'
import ItemShoe from './ItemShoe'
import DetailShoe from './DetailShoe'
export default class ListShoe extends Component {

    renderListShoe = () => {
        return this.props.shoeArr.map((item, index) => {
            return <ItemShoe
                handleClick={this.props.handleChangDetailShoe}
                handClick={this.props.handleAddTocart}
                data={item} key={index} />
        })
    }
    render() {
        return (
            <div className='row'>{this.renderListShoe()}
            </div>

        )
    }
}

