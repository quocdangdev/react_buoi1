import React, { Component } from 'react'
export default class ItemShoe extends Component {
    render() {
        let { image, name, price } = this.props.data;
        return (
            < div className='col-3 p-1' >
                <div className="card text-left h-100">
                    <img className="card-img-top" src={image} alt />
                    <div className="card-body">
                        <h4 className="card-title">{name}</h4>
                        <p className="card-text">giá :{price}$</p>
                    </div>
                    <div>
                        <button onClick={() => {
                            this.props.handleClick(this.props.data)
                            // thông qua props thể lấy dữ liệu từ con DetailShoe
                        }} className='btn btn-secondary'>xem chi tiết</button>
                        <button onClick={() => {
                            this.props.handClick(this.props.data)
                        }} className='btn btn-danger'>thêm mua </button>
                    </div>
                </div>
            </ div>
        )
    }
}
