import React, { Component } from 'react'

export default class CartItem extends Component {
    render() {
        const { item, handelChangeCart } = this.props
        return (
            <>
                <tr>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td><img style={{ width: "50px" }} src={item.image} alt="" /></td>
                    <td>{item.price * item.number} $$</td>
                    <td>
                        <button onClick={() => handelChangeCart(item.id, 'up')} className='btn btn-danger'>+</button>
                        <span>{item.number}</span>
                        <button onClick={() => handelChangeCart(item.id, 'down')} className='btn btn-warning'>-</button>
                    </td>
                </tr>
            </>
        )
    }
}
