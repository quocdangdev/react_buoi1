import React, { Component } from 'react'
import { dataShoe } from './dataShos';
import ItemShoe from './ItemShoe';
import DetailShoe from './DetailShoe';
import Cart from './CartList';
import ListShoe from './ListShoe';

export default class Ex_ShoeShop extends Component {
    state = {
        shoeArr: dataShoe,
        detail: dataShoe[0],
        cart: [],
    }
    handleChangDetailShoe = (shoe) => {
        this.setState({
            detail: shoe,
        })
    }
    handleAddTocart = (shoe) => {
        let cloneCart = [...this.state.cart];
        // Th1 chưa có sản phẩm trong giõ hàng
        //TH2 đã có sản phẩm
        let index = this.state.cart.findIndex((item) => {
            return item.id == shoe.id;
        })
        if (index == -1) {
            // chưa có sp=> thêm mới
            let cartItem = { ...shoe, number: 1 }
            cloneCart.push(cartItem);
        }
        else {
            // sản phẩm đã có trong giõ hàng
            cloneCart[index].number++;
        }

        this.setState({ cart: cloneCart });
    }


    handelChangeCart = (productId, type) => {
        const dataCarts = [...this.state.cart]
        if (!dataCarts.length) return
        const index = dataCarts.findIndex(product => product.id === productId)
        if (index >= 0) {
            if (dataCarts[index].number <= 0 && type === 'down') return
            else {
                dataCarts[index].number = type === 'up' ? dataCarts[index].number + 1 : dataCarts[index].number - 1
                this.setState(prev => ({ ...prev, cart: dataCarts }))
            }
        }
    }

    render() {
        return (
            <div>
                <div className="container">
                    <Cart cart={this.state.cart} handelChangeCart={this.handelChangeCart} />
                    {/* <dhiv className='row'>{this.renderListShoe()}</dhiv> */}
                    <ListShoe
                        handleAddTocart={this.handleAddTocart}
                        handleChangDetailShoe={this.handleChangDetailShoe}
                        shoeArr={this.state.shoeArr} />
                    <DetailShoe
                        detail={this.state.detail} />
                </div>
            </div>
        )
    }
}
