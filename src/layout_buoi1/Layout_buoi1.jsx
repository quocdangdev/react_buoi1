import React, { Component } from 'react'
import Benner from './Benner'
import Boby from './Boby'
import Content from './Content'
import Footer from './Footer'
import Header from './Header'

export default class Layout_buoi1 extends Component {
    render() {
        return (
            <div className='py-0 m-0'>
                <Header />
                <Boby />
                <Benner />
                <Content />
                <Boby />
                <Footer />
            </div>
        )
    }
}
