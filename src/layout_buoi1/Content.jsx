import React, { Component } from 'react'
import styles from "./Color.module.css"
export default class

    extends Component {
    render() {
        return (
            <div className='container'>
                <div className="row color" >
                    <div className={styles.color}>
                        <div className={styles.color_letter}>
                            <div className='col-lg-3 mb-5'>
                                <div className="card" style={{ width: '15rem' }}>
                                    <img className="card-img-top" src="https://kenh14cdn.com/thumb_w/600/2020/4/22/kaka-my-thumb-15875584509861090364746-crop-15875584666651972634554.jpg" alt="Card image cap" />
                                    <div className="card-body">
                                        <h5 className="card-title">Card title</h5>
                                        <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <a href="#" className="btn btn-primary">Go somewhere</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className={styles.color}>
                        <div className={styles.color_letter} >
                            <div className='col-lg-3 mb-5'>
                                <div className="card" style={{ width: '15rem' }}>
                                    <img className="card-img-top" src="https://assets.goal.com/v3/assets/bltcc7a7ffd2fbf71f5/blt409364e1bbbf415e/60df19bd75106a304ca716c1/0986156347c0d3e9e3d33185b8d960cd409deee5.jpg" alt="Card image cap" />
                                    <div className="card-body">
                                        <h5 className="card-title">Card title</h5>
                                        <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <a href="#" className="btn btn-primary">Go somewhere</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className={styles.color}>
                        <div className='col-lg-3 mb-5'>
                            <div className={styles.color_letter}>
                                <div className="card" style={{ width: '15rem' }}>
                                    <img className="card-img-top" src="https://assets.goal.com/v3/assets/bltcc7a7ffd2fbf71f5/bltabd6f5fcb55159b4/60de8fd796e07e0f6e5e4389/028f793adf50bbceb47175a30a7179ceaf80996d.jpg?auto=webp&fit=crop&format=jpg&height=800&quality=60&width=1200" alt="Card image cap" />
                                    <div className="card-body">
                                        <h5 className="card-title">Card title</h5>
                                        <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <a href="#" className="btn btn-primary">Go somewhere</a>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div className={styles.color}>
                        <div className={styles.color_letter}>
                            <div className='col-lg-3 mb-5'>
                                <div className="card" style={{ width: '15rem' }}>
                                    <img className="card-img-top" src="https://assets.goal.com/v3/assets/bltcc7a7ffd2fbf71f5/blt98c1fbbd82cc443d/60de6eb5fd14d30f3eb7c7a0/4587caf178e1f9ceaa38acf1e64176098543a49c.jpg" alt="Card image cap" />
                                    <div className="card-body">
                                        <h5 className="card-title">Card title</h5>
                                        <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <a href="#" className="btn btn-primary">Go somewhere</a>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

        )
    }
}
