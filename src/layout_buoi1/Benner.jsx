import React, { Component } from 'react'
import styles from "./Color.module.css"

export default class Benner extends Component {
    render() {
        return (
            <div className='color container py-4'>
                <div className='p-4 p-lg-5 bg-light rounded-3 '>
                    <div className={styles.color}>
                        <div className='color_letter'>
                            <h2 className={styles.color_letter}>A warm welcome!</h2>
                            <p className={styles.color_letter}>Bootstrap utility classes are used to create this jumbotron since the old component has been removed from the framework. Why create custom CSS when you can use utilities?</p>
                            <button className='btn btn-danger'>call to action</button>
                        </div>

                    </div>
                </div>

            </div >
        )
    }
}
