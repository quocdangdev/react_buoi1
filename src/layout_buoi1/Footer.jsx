import React, { Component } from 'react'
import styles from "./Color.module.css"

export default class Footer extends Component {
    render() {
        return (
            <div className=' bg-dark'>
                <div className="container">
                    <div className={styles.footer}>
                        <h3 className='footer text-center m-0' >Copyright © Your Website 2022</h3>
                    </div>
                </div>

            </div >
        )
    }
}
